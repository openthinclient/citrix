# ----------------------------------------------------------------------------
# Loading progress messages
# ----------------------------------------------------------------------------

loading-progress-title = Loading Citrix resources
loading-progress-resume = Reconnecting sessions
loading-progress-auth = Authenticating
loading-progress-storebrowse = Connecting to store
loading-progress-enumerate = Getting desktops and applications
loading-progress-icons = Creating icons

# ----------------------------------------------------------------------------
# Login messages
# ----------------------------------------------------------------------------

login-title = Citrix Login

# ----------------------------------------------------------------------------
# Configuration error messages
# ----------------------------------------------------------------------------

configuration-error-title = Citrix Configuration error

invalid_hostname = Invalid server name in Storebrowse URL:\n{$url}

no_host = No server name given!

    Please contact your administrator.

no_domain = No domain given!

    Please contact your administrator.

too_many_virtual_channels = Too many virtual channels are activated. Some will not work.

    Deactivate unnecessary virtual channels in the "HDX and multimedia settings", "USB redirection settings" and "Expert settings"

    Please contact your administrator.

invalid_webcam_resolution = Invalid format for "HDX Webcam resolution".

    The default resolution 352x288 will be used.

    Please contact your administrator.

webcam_no_audio = Webcam is enabled but "Audio Input" is disabled. Webcam will not work.

    Please contact your administrator.

invalid_usb_id = USB redirection settings contains invalid USB device ID '{$usb_id}'.

    Please contact your administrator.

unknown_var = Unknown variable "${$var}".

    Please contact your administrator.

# ----------------------------------------------------------------------------
# Error messages
# ----------------------------------------------------------------------------

error-title = Citrix Error

error_stdin_login_data = No login data passed.

    Please contact your administrator.

error_sso_login_data = Single sign-on failed.

    Could not retrieve password and username.

    Please make sure that you have a "Single Sign-on" device, but not a "Autologin" device assigned to the thin client in the openthinclient management.

    Please contact your administrator.

invalid_domain = Invalid domain specified. Domain {$domain} is predefined.

no_domain_entered = No domain specified.

    Please specify a domain.

    Format:  username@domain  or  domain\username

error_copy_custom_config_files = Could not copy custom configuration files.

    {$error}

error_inject_credentials = Failed to inject credentials. (Error code: {$error_code})

    You will be prompted for your credentials.

storebrowse_cert_error = Certificate of Citrix server not trusted.

    A certificate should be uploaded to the openthinclient management.

    Please contact your administrator.

storebrowse_network_error = Could not connect to the server.

    Please check server address, network connection and DNS or contact your administrator.

storebrowse_connection_error = An error occurred while connecting to the server.

    Please contact your administrator.

    Exit code: {$returncode}

    {$stderr}

storebrowse_access_denied = Access denied.

    Please check your credentials.

storebrowse_no_stores = No stores available.

    Please contact your administrator.

error_reconnect = Error while trying to reconnect sessions. (Error code: {$returncode})

    Please contact your administrator.

error_uncaught = An unexpected error occurred.

    {$error}

    Please contact your administrator.
