# ----------------------------------------------------------------------------
# Loading progress messages
# ----------------------------------------------------------------------------

loading-progress-title = Lade Citrix Ressourcen
loading-progress-resume = Stelle Sitzung wieder her
loading-progress-auth = Authentifiziere
loading-progress-storebrowse = Verbinde mit Store
loading-progress-enumerate = Erhalte Desktops und Anwendungen
loading-progress-icons = Erstelle Symbole

# ----------------------------------------------------------------------------
# Login dialog messages
# ----------------------------------------------------------------------------

login-title = Citrix Anmeldung

# ----------------------------------------------------------------------------
# Configuration error messages
# ----------------------------------------------------------------------------

configuration-error-title = Citrix Konfigurationsfehler

invalid_hostname = Ungültiger Servername in Storebrowse-URL:\n{$url}

no_host = Kein Servername angegeben.

    Bitte informieren Sie Ihren Administrator.

no_domain = Keine Domäne angegeben.

    Bitte informieren Sie Ihren Administrator.

too_many_virtual_channels = Zu viele virtuelle Kanäle sind aktiviert. Einige werden nicht funktionieren.

    Deaktivieren Sie unnötige virtuelle Kanäle in "HDX und Multimedia Einstellungen", "USB Weiterleitungs-Einstellungen" and "Experteneinstellungen".

    Bitte informieren Sie Ihren Administrator.

invalid_webcam_resolution = Ungültiges Format in "HDX Webcam Auflösung".

    Die Default-Aufkösung von 352x288 wird verwendet.

    Bitte informieren Sie Ihren Administrator.

webcam_no_audio = Webcam Weiterleitung ist aktiviert, aber Audio Eingabe ist deaktiviert. Die Webcam wird nicht funktionieren.

    Bitte informieren Sie Ihren Administrator.

invalid_usb_id = USB Weiterleitungs-Einstellungen enthält ungültige USB-Geräte-ID '{$usb_id}'.

    Bitte informieren Sie Ihren Administrator.

unknown_var = Unbekannte Variable "${$var}".

    Bitte informieren Sie Ihren Administrator.

# ----------------------------------------------------------------------------
# Error messages
# ----------------------------------------------------------------------------

error-title = Citrix Fehler

error_stdin_login_data = Es wurden keine Anmeldeinformationen übergeben.

    Bitte informieren Sie Ihren Administrator.

error_sso_login_data = Single Sign-on gescheitert.

    Benutzername und Passwort konnten nicht ausgelesen werden.

    Bitte überprüfen Sie, dass dem ThinClient ein "Single Sign-on" Gerät, aber kein "Autologin" Gerät im openthinclient-Management zugewiesen ist.

    Bitte informieren Sie Ihren Administrator.

invalid_domain = Ungültige Domäne angegeben. Domäne {$domain} ist voreingestellt.

no_domain_entered = Keine Domäne eingegeben.

    Bitte geben Sie eine Domäne ein.

    Format:  benuzter@domäne  oder  domäne\benuzter

error_copy_custom_config_files = Konnte Custom Konfigurationsdateien nicht kopieren.

    {$error}

error_inject_credentials = Übergeben der Anmeldeinformationen an Citrix gescheitert. (Fehlercode: {$error_code})

    Sie werden nach Ihren Anmeldeinformationen gefragt.

storebrowse_cert_error = Dem Zertifikat des Servers wird nicht vertraut.

    Ein Zertifikat sollte im openthinclient-Management hinzugefügt werden.

    Bitte informieren Sie Ihren Administrator.

storebrowse_network_error = Verbindung mit Server fehlgeschlagen.

    Bitte überprüfen Sie die Serveradresse, die Netzwerkverbindung und DNS oder informieren Sie Ihren Administrator.

storebrowse_connection_error = Fehler beim Verbinden mit Server.

    Bitte informieren Sie Ihren Administrator.

    Exit Code: {$returncode}

    {$stderr}

storebrowse_access_denied = Zugriff verweigert.

    Bitte überprüfen Sie Ihre Anmeldeinformationen.

storebrowse_no_stores = Keine Stores gefunden.

    Bitte überprüfen Sie Ihre Anmeldeinformationen.

error_reconnect = Fehler veim Versuch Sitzung wieder zu verbinden. (Fehlercode: {$returncode})

    Bitte informieren Sie Ihren Administrator.

error_uncaught = Ein unerwarteter Fehler ist aufgetreten.

    {$error}

    Bitte informieren Sie Ihren Administrator.
