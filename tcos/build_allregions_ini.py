import configparser
import os

import tcos.logger

from common import ICA_ROOT, ICA_HOME

ALL_REGIONS_INI = f'{ICA_HOME}/All_Regions.ini'
ALL_REGIONS_INI_TMPL = f'{ICA_ROOT}/config/All_Regions.ini'

LOG = tcos.logger.get()


def build_allregions_ini(settings):
    get_str = settings.get_str

    LOG.debug('Building All_Regions.ini')
    config = configparser.ConfigParser()
    config.optionxform = str
    config.read(f'{ICA_ROOT}/config/All_Regions.ini')

    connection_bar = get_str('ExpertSettings.ConnectionBar')
    config['Client Engine\\GUI']['ConnectionBar'] = connection_bar

    if get_str('ExpertSettings.UseCefBrowser') == 'On':
        LOG.debug('Activating Cef in All_Regions.ini')
        config['Client Engine\\WebPageRedirection']['UseCefBrowser'] = 'true'
    else:
        LOG.debug('Deactivating Cef in All_Regions.ini')
        config['Client Engine\\WebPageRedirection']['UseCefBrowser'] = '*'

    with open(ALL_REGIONS_INI, 'w') as ini_file:
        config.write(ini_file)
