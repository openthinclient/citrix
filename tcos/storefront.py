import os
import psutil
import random
import requests
import string

from subprocess import DEVNULL
from urllib.parse import urljoin, urlsplit

import tcos.logger
from tcos.utils import run

from common import ICA_ROOT

LOG = tcos.logger.get()


def resume_sessions(settings, login_conf):
    """ Try session resume with the StoreFront Web API

        Returns a set of psutil processes that were started to resume sessions.
    """
    # API docs:
    # https://developer-docs.citrix.com/en-us/storefront/storefront-web-api

    LOG.debug('Trying fast session resume...')

    # Reduce cerficate warnings
    import warnings

    from urllib3.exceptions import (InsecureRequestWarning,
                                    SubjectAltNameWarning)
    warnings.simplefilter('once', SubjectAltNameWarning)
    warnings.simplefilter('once', InsecureRequestWarning)

    url = login_conf['host']

    if not url.startswith('http'):
        url = 'https://' + url

    url_parts = urlsplit(url)
    host = url_parts.scheme + '://' + url_parts.netloc

    # Generate device ID
    # This identifies where active sessions (that should be resumed) are
    # running. Only active sessions that run on another device ID are
    # resumed.
    # The device ID consists of (part of) the hostname (for easier recognition
    # in any logs) and random characters (so that sessions which are not
    # properly disconnected, i.e. are still active, can be resumed, e.g.
    # after a crash or hard reset).
    # The device ID must not be longer than 20 characters, else
    # Sessions/ListAvailable refuses with a 500 error.
    proc = run('/usr/bin/hostname', stdout=True)
    if proc.returncode == 0:
        hostname_part = (proc.stdout.strip()[:16],)
    else:
        LOG.warning('Failed to get hostname')
        hostname_part = random.choices(string.ascii_lowercase, k=16)
    DEVICE_ID = ''.join((
            *hostname_part,
            *random.choices(string.ascii_lowercase, k=4),
    ))

    # Path to the certificate bundle, produced by citrix-setup
    CERT_PATH = '/opt/Citrix/ICAClient/keystore/cacerts-bundle.crt'

    # Get StoreFront path from / redirect
    # (This requires a default StroreFront site to be configured.)
    response = requests.request(
            'GET', host, verify=CERT_PATH, allow_redirects=False)
    if response.status_code != 302:
        LOG.error('Failed to acquire StoreFront path: %s',
                  response.status_code)
        raise Exception('Failed to acquire StoreFront path')
    BASE_URL = urljoin(host, response.headers['Location'])


    # StoreFront API request helper.
    # Keeps control of CSRF token and cookies (incl. the auth cookie).
    cookies = {
        'CtxsDeviceId': DEVICE_ID,
    }

    def request(method, url, **data):
        headers = {
            'X-Citrix-IsUsingHTTPS': 'Yes',
        }
        if 'CsrfToken' in cookies:
            headers['Csrf-Token'] = cookies['CsrfToken']

        url = '{BASE_URL}/{url}'.format(BASE_URL=BASE_URL, url=url)

        response = requests.request(
            method,
            url,
            headers=headers,
            cookies=cookies,
            verify=CERT_PATH,
            data=data
        )
        if response.status_code != 200:
            LOG.error('Request failed: %s %s', response.status_code, url)
            raise Exception('Request failed: %s %s' % (response.status_code, url))

        cookies.update(response.cookies)
        return response

    # The actual resume code

    request(
        'POST', 'PostCredentialsAuth/Login',
        username=f'{login_conf["user"]}@{login_conf["domain"]}',
        password=login_conf['password'],
    )

    noActive = (settings.reconnect == 'fast_disconnected') and 'true' or 'false'
    sessions = request(
        'POST', 'Sessions/ListAvailable', excludeActive=noActive,
    ).json()

    procs = set()
    for session in sessions or ():
        name = session['initialapp']
        LOG.info('Resuming %s', name)

        ica_content = request('GET', session['launchurl']).text
        ica_path = os.path.join('/tmp', '{name}.ica'.format(name=name))
        with open(ica_path, 'w') as ica_file:
            ica_file.write(ica_content)

        procs.add(psutil.Popen(
            (
                '/opt/Citrix/ICAClient/wfica',
                '-icaroot', ICA_ROOT,
                ica_path
            ),
            stdout=DEVNULL, stderr=DEVNULL,
        ))

    return procs
