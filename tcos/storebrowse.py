from collections import namedtuple
import os
import re
import shlex
import subprocess

from tcos import logger
from tcos.utils import run

from common import (
    AuthenticationFailedException,
    DESKTOP_FILES_PATH,
    DROP_TCOSD,
    ICA_HOME, ICA_ROOT,
    show_error
)

# Template of the .desktop files for Citrix applications and desktops
DESKTOP_FILE_TMPL = """[Desktop Entry]
Comment={id}
Name={name}
Exec={cmd}
Path=/opt/Citrix
Type=Application
Categories=Application;TCOS;
Icon={icon}
StartupNotify=true
"""

NON_DIGIT_RE = re.compile(r'\D')

Resource = namedtuple('Resource', 'id name path icon')

class StoreBrowse:
    store_url = ''
    selfservice_proc = None

    def __init__(self, url, settings):
        self.url = url
        self.settings = settings
        self.suppress_error = settings.restart_login
        self.resources = []

    def _connection_timeouts(self, op):
        timeouts = self.settings.get(f'ExpertSettings.timeouts.{op}', '')
        for timeout in filter(None, NON_DIGIT_RE.split(timeouts)):
            yield int(timeout)
        while True:
            yield None

    def run(self, *cli_args, timeout=None):
        """ Run storebrowse with given arguments.

            Return process object or None on error.

            An error dialog is shown on error.
        """
        proc = run(
            f'{ICA_ROOT}/util/storebrowse', *cli_args,
            timeout=timeout,
            stdout=True, stderr=subprocess.STDOUT
        )

        output = proc.stdout
        show = show_error if not self.suppress_error else lambda *a, **k: None

        if 'AM_ERROR_AUTH_ACCESS_DENIED' in output:
            show('storebrowse_access_denied')
            raise AuthenticationFailedException()
        elif 'AM_ERROR_AUTH_CANCELLED_BY_USER' in output:
            pass
        elif 'AM_ERROR_AUTH_NETWORK_ERROR' in output:
            show('storebrowse_network_error')
        elif (proc.returncode == 150
                or 'AM_ERROR_HHTP_SERVER_CERTIFICATE_NOT_TRUSTED' in output):
            show('storebrowse_cert_error')
        elif proc.returncode:
            show('storebrowse_connection_error',
                 returncode=proc.returncode,
                 stderr=output )
        else: # i.e. no errors or timeout (returncode is None)
            return proc

        return None

    def start_selfservice(self):
        if self.selfservice_proc:
            return
        self.selfservice_proc = subprocess.Popen((
            DROP_TCOSD, f'{ICA_ROOT}/selfservice', '--background'
        ))

    def restart_selfservice(self):
        if self.selfservice_proc:
            self.selfservice_proc.kill()
            self.selfservice_proc.wait()
            self.selfservice_proc = None
        self.start_selfservice()

    def get_store_url(self):
        """ Get store URL from storebrowse.

            Return store URL or None on error.

            An error dialog is shown on error.
        """
        # Skip --addstore if magic files have been copied from custom config.
        skip_addstore = (
                os.path.exists(f'{ICA_HOME}/cache/Stores/StoreCache.ctx')
            and os.path.exists(f'{ICA_HOME}/config/ServiceRecord.xml')
        )

        if not skip_addstore:
            if not self.run('--addstore', self.url):
                return

        proc = self.run('--liststores')
        if not proc:
            return

        first_line = proc.stdout.lstrip().split('\n',1)[0]
        if not first_line:
            show_error('storebrowse_no_stores')
            return

        self.store_url = shlex.split(first_line)[0]
        return self.store_url

    def get_resources(self):
        """ Get store resources (apps and desktops) from storebrowse.

            Return list of objects with .id, .name, .path, .icon.

            If an error occurs, show an error dialog and return empty list.

        """
        resources = []

        if not self.store_url:
            if not self.get_store_url():
                return resources

        # Explicitly starting selfservice (instead of letting storebrowse do
        # it) magically accelerates the subsequent --enumerate. (Citrix
        # Workspace 2311). It also let's selfservice run with reduced
        # privileges. While storebrowse may hang if started via `DROP_TCOSD`
        # (for whatever reason).
        self.start_selfservice()

        # Enumerate resources (Retry if --enumerate hangs.)
        for timeout in self._connection_timeouts('enumerate'):
            proc = self.run(self.store_url, '--enumerate', '--icons', 'best',
                            timeout=timeout)
            if not proc:  # error detected and shown in run()
                return resources
            elif proc.returncode is not None:  # not a timeout
                break
            logger.warning('Enumerate timed out after %.2f seconds', timeout)
            self.restart_selfservice()

        # Parse enumerated resources
        #
        # Format:
        # Lines of tab-separated and single-quoted values (Resource name,
        # Display name, Resource folder and path of the best icon), e.g.:
        #
        #    'otc-ctxstudio:Notepad'\t'Notepad'\t'\\'\t'/root/.ICAClient/cache/Icons/otc-ctxstudio:Notepad.png'
        for line in proc.stdout.splitlines():
            parts = shlex.split(line)
            if len(parts) == 4: # skip invalid lines
                resources.append(Resource(*parts))

        self.resources = resources
        return resources

    def reconnect_sessions(self, no_active):
        """ Reconnect to existing sessions. """
        # Reconnect must be done after the --enumerate command in
        # `.get_resources()` (Citrix Workspace 2212)
        if not self.resources:
            if not self.get_resources():
                return

        param = '-Wr' if no_active else '-WR'
        # The reconnect may hang. Retry if it does.
        for timeout in self._connection_timeouts('reconnect'):
            proc = self.run(self.store_url, param, timeout=timeout)
            if not proc:
                return
            elif proc.returncode is not None:  # not a timeout
                break
            self.restart_selfservice()
            self.get_resources()  # re-enumerate (see above for motivation)

    def launch_resource(self, resource):
        """ Launch a resource (app or desktop) from the store. """

        if not self.store_url:
            if not self.get_store_url():
                return

        return subprocess.Popen((
            DROP_TCOSD,
            f'{ICA_ROOT}/util/storebrowse', self.store_url,
            '--launch', resource.id
        ))

    # match special chars that need to be quotet in *Exec= lines
    _quote_re = re.compile(r'(["`$\\])')

    def create_desktop_icon(self, res):
        # Note: DESKTOP_FILES_GLOB must match the .desktop file names
        desktop_file_path = f'{DESKTOP_FILES_PATH}/tcos_citrix-{res.id}.desktop'
        os.makedirs(DESKTOP_FILES_PATH, exist_ok=True)

        cmd = (
            f'{ICA_ROOT}/util/storebrowse', self.store_url,
            '--launch', res.id
        )
        # quote all parts of Exec= line (see Desktop Entry Specification:
        # specifications.freedesktop.org/desktop-entry-spec/latest/ar01s07.html)
        quote = lambda str: '"{}"'.format(      # wrap in double quotes
            self._quote_re.sub(r'\\\1', str)    # escape special chars
            .replace('\\', r'\\')               # escape backslashes (again)
        )
        cmd = ' '.join(map(quote, cmd))

        with open(desktop_file_path, 'w') as desktop_file:
            desktop_file.write(DESKTOP_FILE_TMPL.format(
                id=res.id,
                name=res.name,
                icon=res.icon,
                cmd=cmd,
            ))
        # Mark as executable because Mate wont display icons without +x
        os.chmod(desktop_file_path, 0o755)
        # Install desktop icon (i.e. add to desktop and menu). Just copying
        # the file to the desktop can result in broken (placeholder) icons.
        run('xdg-desktop-icon', 'install', desktop_file_path)
