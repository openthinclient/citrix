import os
import tcos.logger

from common import ICA_HOME

FILEACCESS_PATH = f'{ICA_HOME}/.fileaccess'

# Format of .fileaccess: url padded with \0 to 128 bytes + permission bytes
FILEACCESS_FMT = '{store_url:\0<128}{permission}'
PERMISSION_BYTES = {
    'ask':       '\0\0\0\1',
    'deny':      '\1\0\0\1',
    'readonly':  '\2\0\0\1',
    'readwrite': '\3\0\0\1',
}

LOG = tcos.logger.get('citrix/build_fileaccess')

def build_fileaccess(settings, store_url):
    """ Write .fileaccess files as if the user had selected permissions. """

    fileaccess = settings.get('ClientDrives.FileAccess')
    permission_bytes = PERMISSION_BYTES.get(fileaccess)
    if not permission_bytes:
        LOG.error(f'Invalid fileaccess setting: {fileaccess}')
        if os.path.isfile(FILEACCESS_PATH):
            os.remove(FILEACCESS_PATH)
        return

    with open(FILEACCESS_PATH, 'wb') as fileaccess_file:
        fileaccess_file.write(
            FILEACCESS_FMT.format(store_url=store_url,
                                    permission=permission_bytes
                            ).encode('utf-8')
        )
