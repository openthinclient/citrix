# Grab bag of constants, functions and classes used throughout the code

ICA_ROOT = '/opt/Citrix/ICAClient'
ICA_HOME = '/home/tcos/.ICAClient'

DESKTOP_FILES_PATH = '/home/tcos/.local/share/applications'
DESKTOP_FILES_GLOB = f'{DESKTOP_FILES_PATH}/tcos_citrix-*.desktop'
def get_desktop_filename(name):
    return f'{DESKTOP_FILES_PATH}/tcos_citrix-{name}.desktop'

# Path to the command for running without the tcosd supplementary group
DROP_TCOSD = '/usr/local/bin/tcos-drop-tcosd'

class AuthenticationFailedException(Exception):
    pass


# Dialog shortcuts

import tcos.dialog

def open_detached_dialog(type, **kwargs):
    return tcos.dialog.DetachedDialog(
        dir='/opt/citrix/tcos/dialogs', type=type, **kwargs)

show_dialog = lambda type, **kwargs: tcos.dialog.open(
        dir='/opt/citrix/tcos/dialogs', type=type, **kwargs)

def show_config_error(message_key, **values):
    show_dialog('config-error', message_key=message_key, values=values)

def show_error(message_key, **values):
    show_dialog('error', message_key=message_key, values=values)

def close_dialog(dialog_id):
    tcos.dialog.destroy(dialog_id)
