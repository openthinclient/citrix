import os
import re
import shutil
from itertools import chain

import tcos.usb
import tcos.logger
from common import ICA_ROOT, show_config_error

LOG = tcos.logger.get('citrix/build_usb_conf')

USB_CONF = f'{ICA_ROOT}/usb.conf'
USB_CONF_TMPL = f'{ICA_ROOT}/usb.conf.tmpl'

# Helper functions

is_valid_usb_id = re.compile(r'^[0-9A-Fa-f]{4}(:[0-9A-Fa-f]{4})?$').match

# Build a line with a policy rule
def policy_rule(op, identifier, comment=None):
    if comment:
        return f'{op}: {identifier}  # {comment}\n'
    return f'{op}: {identifier}\n'
DENY = lambda expr, comment=None: policy_rule('DENY',  expr, comment)

# Build device identifier expression froms list of USB IDs (VID:PID or just VID)
def device_identifiers(usb_ids):
    for usb_id in usb_ids:
        if ':' in usb_id:
            vid, pid = usb_id.split(':')
            yield f'vid={vid} pid={pid}'
        else:
            yield f'vid={usb_id}'

# Building the usb.conf file

# Custom rules from the citrix configuration
def filter_custom_usb_ids(usb_ids_str):
    for usb_id in usb_ids_str.split():
        if not is_valid_usb_id(usb_id):
            LOG.info(f'Ignoring invalid USB ID "{usb_id}"')
            show_config_error('invalid_usb_id', usb_id=usb_id)
            continue
        yield usb_id

def custom_rules(op, usb_ids_str):
    for expr in device_identifiers(filter_custom_usb_ids(usb_ids_str)):
        yield policy_rule(op, expr, 'Custom rule')


# Disable USB redirection for devices that are redirected via virtual channels
def get_virtual_channel_rules(settings):
    if settings.grundig_enabled:
        for expr in device_identifiers(tcos.usb.GRUNDIG_DEVICE_IDS):
            yield DENY(expr, 'Grundig devices redirected via virtual channel')
    if settings.olympus_enabled:
        for expr in device_identifiers(tcos.usb.OLYMPUS_DEVICE_IDS):
            yield DENY(expr, 'Olympus devices redirected via virtual channel')
    if settings.philips_enabled:
        for expr in device_identifiers(tcos.usb.PHILIPS_DEVICE_IDS):
            yield DENY(expr, 'Philips devices redirected via virtual channel')
    if settings.signotec_enabled:
        for expr in device_identifiers(tcos.usb.SIGNOTEC_DEVICE_IDS):
            yield DENY(expr, 'Signotec devices redirected via virtual channel')
    if settings.get('HDX.EnableWebcam') != 'ctxusb':
        yield DENY('class=0e', 'Webcam either redirected via HDX or off')


# Exclude all mass storage devices if generic USB redirection is disabled
def get_massstorage_usb_conf_rule(settings):
    if settings.get('USBredirection.genericUSBRedirection') == 'NonMassStorage':
        yield DENY('class=08', 'Exclude all mass storage devices')


# API function to build the usb.conf file
def build_usb_conf(settings):
    LOG.debug('Writing usb.conf')

    if os.path.exists(USB_CONF_TMPL):
        shutil.copyfile(USB_CONF_TMPL, USB_CONF)
    else:
        shutil.copyfile(USB_CONF, USB_CONF_TMPL)

    usb_conf_rules = chain(
        *custom_rules('DENY', settings.get_str('USBredirection.DenyDevices')),
        *get_virtual_channel_rules(settings),
        *get_massstorage_usb_conf_rule(settings),
        *custom_rules('CONNECT',
                      settings.get_str('USBredirection.ConnectDevices')),
    )

    with open(USB_CONF, 'a') as usb_conf_file:
        usb_conf_file.write('\n'
            '# Added by the openthinclient citrix launcher\n')
        for rule in usb_conf_rules:
            usb_conf_file.write(rule)
        usb_conf_file.write('\nALLOW: # all other devices\n')
