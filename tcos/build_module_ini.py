import configparser
import logging
import os
import re
import shutil

import tcos.logger

from common import ICA_ROOT, show_config_error

MODULE_INI = f'{ICA_ROOT}/config/module.ini'
MODULE_INI_TMPL = f'{ICA_ROOT}/config/module.ini.tmpl'

LOG = tcos.logger.get()

split_list = re.compile(r'\s*,\s*').split

def build_module_ini(settings):
    LOG.debug('Building module.ini')

    if not os.path.exists(MODULE_INI_TMPL):
        shutil.copyfile(MODULE_INI, MODULE_INI_TMPL)

    module_ini = configparser.ConfigParser()
    module_ini.optionxform = str
    module_ini.read(MODULE_INI_TMPL)

    virtualdrivers = set(split_list(module_ini['ICA 3.0']['VirtualDriver']))

    # Philips - recreate what the installer does (even if it seems silly)
    if settings.philips_enabled:
        LOG.info('Activating Philips VC')

        virtualdrivers.update(('PSPH', 'PSPH2', 'PSPA', 'PSPM'))

        module_ini['ICA 3.0']['SpeechMike'] = 'Off'

        module_ini['PSPDPM'] = {
            'DriverName': 'VDPSPDPM.dll',
            'LIB_DIR': f'{ICA_ROOT}/SpMikeLib',
            'LIB_NAME': 'libCtxHidMan.so',
            'DPM_DIR': '/tmp/PhilipsDPM',
            'DPM_DRIVE': 'P:\\',
        }
        module_ini['ICA 3.0']['PSPDPM'] = 'Off'

        module_ini['PSPA'] = {
            'DriverName': 'VDPSPAUD.dll'
        }
        module_ini['ICA 3.0']['PSPA'] = 'On'

        module_ini['SpeechMikeAudio'] = {
            'LIB_DIR': f'{ICA_ROOT}/SpMikeLib',
            'LIB_NAME': 'libCtxSbExtAlsa.so'
        }
        module_ini['ICA 3.0']['SpeechMikeAudio'] = 'Off'

        module_ini['PSPM'] = {
            'DriverName': 'VDPSPMIX.dll'
        }
        module_ini['ICA 3.0']['PSPM'] = 'On'

        module_ini['SpeechMikeMixer'] = {
            'LIB_DIR': f'{ICA_ROOT}/SpMikeLib',
            'LIB_NAME': 'libCtxMixerAlsa.so'
        }
        module_ini['ICA 3.0']['SpeechMikeMixer'] = 'Off'

        module_ini['PSPH'] = {
            'DriverName': 'VDPSPHID.dll'
        }
        module_ini['ICA 3.0']['PSPH'] = 'On'

        module_ini['PSPH2'] = {
            'DriverName': 'VDPSPHID2.dll'
        }
        module_ini['ICA 3.0']['PSPH2'] = 'On'

        module_ini['PSPHID'] = {
            'LIB_DIR': f'{ICA_ROOT}/SpMikeLib',
            'LIB_NAME': 'libCtxHIDManagerRemoteClient.so'
        }

        module_ini['PSPHID2'] = {
            'LIB_DIR': f'{ICA_ROOT}/SpMikeLib',
            'LIB_NAME': 'libCtxHIDManagerRemoteClient.so',
            'CH': '2',
        }


    # Grundig
    if settings.grundig_enabled:
        LOG.info('Activating Grundig VC')
        virtualdrivers.add('GRUMMC')
        module_ini['ICA 3.0']['GRUMMC'] = 'On'
        module_ini['GRUMMC'] = {
            'DriverName': 'GRUMMC.DLL'
        }

    # Olympus / OM Digital Solutions
    if settings.olympus_enabled:
        LOG.info('Activating Olympus VC')
        virtualdrivers.add('OlyCom')
        module_ini['ICA 3.0']['OlyCom'] = 'On'
        module_ini['OlyCom'] = {
            'DriverName': 'vdolycom.so'
        }

    # Signotec
    if settings.signotec_enabled:
        LOG.info('Activating Signotec VC')
        virtualdrivers.add('STvcpad')
        module_ini['ICA 3.0']['STvcpad'] = 'On'
        module_ini['STvcpad'] = {
            'DriverName': 'stvcpad.so'
        }

    # Zoom VC
    if settings.get('HDX.zoom') == 'True':
        LOG.info('Activating Zoom VC')
        virtualdrivers.add('ZoomMedia')
        module_ini['ZoomMedia'] = {'DriverName': 'ZoomMedia.so'}

    # Webex TeamsVDI
    if settings.get('HDX.webex') == 'True':
        LOG.info('Activating Webex Teams VDI driver')
        virtualdrivers.add('CiscoTeamsVirtualChannel')
        module_ini['CiscoTeamsVirtualChannel'] = {
            'DriverName': 'libCiscoTeamsCitrixPlugin.so'
        }
        module_ini['ICA 3.0']['CiscoTeamsVirtualChannel'] = 'On'

    # TUI (transparent UI)
    if settings.get('ExpertSettings.VDTUI') != 'On':
        LOG.info('Deactivating VDTUI driver.')
        virtualdrivers.discard('VDTUI')
        module_ini.remove_option('ICA 3.0', 'VDTUI')

    # NSAP (NetScaler Application Experience) feature
    if settings.get('ExpertSettings.VDNSAP') != 'On':
        virtualdrivers.discard('VDNSAP')
        module_ini.remove_option('ICA 3.0', 'VDNSAP')
        LOG.info('Deactivating VDNSAP driver.')

    # WEBRTC
    if settings.get('HDX.VDWEBRTC') != 'On':
        virtualdrivers.discard('VDWEBRTC')
        module_ini.remove_option('ICA 3.0', 'VDWEBRTC')
        LOG.info('Deactivating VDWEBRTC driver.')

    # Configures HDX Realtime Media Engine
    if settings.get('HDX.VCRTME') == 'On':
        virtualdrivers.add('HDXRTME')
        module_ini['ICA 3.0']['HDXRTME'] = 'On'
        module_ini['HDXRTME'] = {'DriverName': 'HDXRTME.so'}
        LOG.info('Activating HDXRTME driver.')

    # Generic USB Redirection
    if settings.get('USBredirection.genericUSBRedirection') != 'True':
        virtualdrivers.discard('GenericUSB')
        module_ini['ICA 3.0']['GenericUSB'] = 'Off'

    # Activate HDX MultiStream Windows Media Redirection
    if settings.get('HDX.WindowsMediaRedirection') != 'On':
        virtualdrivers.discard('MultiMedia')
        module_ini['ICA 3.0']['MultiMedia'] = 'Off'

    if len(virtualdrivers) > 64:
        LOG.warning('Too many virtual channels (%d): %s',
                    len(virtualdrivers), ', '.join(virtualdrivers))
        show_config_error('too_many_virtual_channels')

    module_ini['ICA 3.0']['VirtualDriver'] = ', '.join(virtualdrivers)


    # Disables Customer-Experience-"Something"
    module_ini['CEIP']['EnableCeip'] = 'Disable'

    conf = {
        # Extended Unicode Keyboard support.
        'Keyboard.UseEUK': ('WFClient', 'UseEUKS'),
        # Audio
        'HDX.UDPAudio': ('ClientAudio', 'EnableUDPAudio'),
        'HDX.UDPAudioPortLow': ('ClientAudio', 'UDPAudioPortLow'),
        'HDX.UDPAudioPortHigh': ('ClientAudio', 'UDPAudioPortHigh'),
        'HDX.EnableUDPThroughGateway': ('WFClient', 'EnableUDPThroughGateway'),
        'HDX.VdcamVersion4Support': ('ClientAudio', 'AudioRedirectionV4'),
        # Desktop Appliance Mode
        'ExpertSettings.DesktopApplianceMode':
                ('WFClient', 'DesktopApplianceMode'),
    }
    for key, (section, option) in conf.items():
        module_ini[section][option] = settings.get_str(key)

    # Logging
    syslog_threshold = '3' if LOG.level == logging.DEBUG else '7'
    module_ini['WFClient']['SyslogThreshold'] = syslog_threshold

    with open(MODULE_INI, 'w') as configfile:
        module_ini.write(configfile)
