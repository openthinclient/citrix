import configparser
import os

import tcos.logger

from common import ICA_HOME, ICA_ROOT, show_config_error

WFCLIENT_INI = f'{ICA_HOME}/wfclient.ini'
WFCLIENT_INI_TMPL = f'{ICA_ROOT}/config/wfclient.template'

MAXCLIENTDRIVES = 2
MAXCOMPORTS = 3

LOG = tcos.logger.get()


def build_wfclient_ini(settings):
    LOG.debug('Building wfclient.ini')

    wfclient_ini = configparser.ConfigParser()
    wfclient_ini.optionxform = str
    wfclient_ini.read(WFCLIENT_INI_TMPL)

    # [WFClient] section
    wfclient = wfclient_ini['WFClient']

    conf = {
        # Expert settings
        'ExpertSettings.FontSettings.FontSmoothingType': 'FontSmoothingType',
        'ExpertSettings.AutoReconnect.Activate': 'TransportReconnectEnabled',
        'ExpertSettings.AutoReconnect.TransportReconnectRetries':
                'TransportReconnectRetries',
        'ExpertSettings.AutoReconnect.TransportReconnectDelay':
                'TransportReconnectDelay',
        'ExpertSettings.DisableSound': 'DisableSound',
        'ExpertSettings.EnableOSS': 'EnableOSS',
        'ExpertSettings.TWRedundantImageItems': 'TWRedundantImageItems',

        # Keyboard settings
        'Keyboard.KeyboardLayout':     'KeyboardLayout',
        'Keyboard.ClientKeyboardType': 'KeyboardMappingFile',
        'Keyboard.ServerKeyboardType': 'KeyboardType',
        'Keyboard.TransparentKeyPassthrough': 'TransparentKeyPassthrough',
        'Keyboard.MouseSendsControlV': 'MouseSendsControlV',

        # Client drive mapping
        'ClientDrives.CDMAllowed': 'CDMAllowed',
        'ClientDrives.DynamicCDM': 'DynamicCDM',

        # Client printer
        'LocalPrinter.Activate':             'ClientPrinter',
        'LocalPrinter.DefaultPrinter':       'DefaultPrinter',
        'LocalPrinter.DefaultPrinterDriver': 'DefaultPrinterDriver',
        'LocalPrinter.ClientPrinterList':    'ClientPrinterList',
    }
    for key, option in conf.items():
        wfclient[option] = settings.get_str(key)

    # Enables Audio / Audio Input
    if settings.get('HDX.AudioMapping') == 'Off':
        wfclient['ClientAudio'] = 'Off'
    else:
        wfclient['ClientAudio'] = 'On'
        wfclient['AudioBandwidthLimit'] = settings.get_str(
                'HDX.AudioMapping')

    allow_audio_input = settings.get('HDX.AllowAudioInput')
    wfclient['AllowAudioInput']  = allow_audio_input
    wfclient['EnableAudioInput'] = allow_audio_input

    # HDX Window Media Redirection.
    # This only affects video playback in Windows Media Player. We need the
    # ability to turn this off since a wrong codec can break usability of
    # Windows Media Player.
    if settings.get('HDX.WindowsMediaRedirection') == 'On':
        wfclient['SpeedScreenMMA'] = 'On'
        wfclient['SpeedScreenMMAVideoEnabled'] = 'True'
        wfclient['SpeedScreenMMAAudioEnabled'] = 'True'
    else:
        wfclient['SpeedScreenMMA'] = 'Off'
        wfclient['SpeedScreenMMAVideoEnabled'] = 'False'
        wfclient['SpeedScreenMMAAudioEnabled'] = 'False'

    # Configures webcam settings
    if settings.get('HDX.EnableWebcam') == 'HDX':
        wfclient['HDXWebCamEnabled']    = 'True'
        wfclient['HDXH264InputEnabled'] = 'True'
    else:
        wfclient['HDXWebCamEnabled'] = 'False'

    conf = {
        'HDX.WebcamFPS':           'HDXWebCamFramesPerSec',
        'HDX.WebcamDevice':        'HDXWebCamDevice',
        'HDX.HDXH264EnableNative': 'HDXH264EnableNative',
    }
    for key, option in conf.items():
        wfclient[option] = settings.get_str(key)

    # Gets Webcam resolution. Uses default values if failed to parse.
    res = settings.get_str('HDX.WebcamResolution')
    res = [x.strip() for x in res.split('x')]
    if len(res) != 2 or not all(x.isdigit() for x in res):
        LOG.warning('Could not parse HDX Webcam resolution.')
        show_config_error('invalid_webcam_resolution')
        res = ['352', '288']
    wfclient['HDXWebCamWidth'], wfclient['HDXWebCamHeight'] = res

    if (settings.get('HDX.EnableWebcam') != 'False'
            and settings.get('HDX.AllowAudioInput') == 'False'):
        show_config_error('webcam_no_audio')

    # Drive mappings
    for i in range(1, MAXCLIENTDRIVES + 1):
        letter = settings.get(f'ClientDrives.{i}.WindowsDrive', '').rstrip(':')
        path   = settings.get(f'ClientDrives.{i}.LocalPath', '')
        if letter.isalpha() and os.path.isdir(path):
            letter = letter[0].upper()
            wfclient[f'DriveEnabled{letter}'] = 'True'
            wfclient[f'DrivePath{letter}']    = path

    # COM-ports
    count = 0
    for i in range(1, MAXCOMPORTS + 1):
        com_port = settings.get(f'ComPorts.{i}', 'none')
        if com_port != 'none':
            count += 1
            wfclient[f'ComPort{count}'] = com_port
    if count:
        wfclient['LastComPortNum'] = str(count)


    # Redirects Philips Speechmike
    if settings.philips_enabled:
        LOG.info('Activating Philips USB redirection')
        wfclient['DrivePathP']    = '/tmp/PhilipsDPM'
        wfclient['DriveEnabledP'] = 'True'

        wfclient['DrivePathS']    = '/tmp/PhilipsSpeechAir'
        wfclient['DriveEnabledS'] = 'True'


    # [Thinwire3.0] section
    thinwire3 = wfclient_ini['Thinwire3.0']

    conf = {
        # Expert settings
        'ExpertSettings.Tw2CachePower':     'Tw2CachePower',
        'ExpertSettings.AllowBackingStore': 'AllowBackingStore',
        'ExpertSettings.UseServerRedraw':   'UseServerRedraw',
        'ExpertSettings.ApproximateColors': 'ApproximateColors',

        # Fixes black box around mouse cursor
        'Keyboard.DisableXRender': 'DisableXrender',

        # TLS Version settings
        'ExpertSettings.TLS.minimumTLS': 'MinimumTLS',
        'ExpertSettings.TLS.maximumTLS': 'MaximumTLS',

        # Bitmap cache
        'ExpertSettings.Bitmapcache.Activate': 'PersistentCacheEnabled',
    }
    for key, option in conf.items():
        thinwire3[option] = settings.get_str(key)

    cache_size = settings.get_str('ExpertSettings.Bitmapcache.Size')
    if not cache_size.isdigit():
        cache_size = 10
    thinwire3['PersistentCacheSize'] = str(int(cache_size) * 1024 * 1024)

    cache_min = settings.get_str('ExpertSettings.Bitmapcache.MinBitmapSize')
    if not cache_min.isdigit():
        cache_min = 0
    thinwire3['PersistentCacheMinBitmap'] = str(int(cache_min) * 1024)

    with open(WFCLIENT_INI, 'w') as configfile:
        wfclient_ini.write(configfile)
