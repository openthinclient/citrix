package Os_Citrix.buildTypes

import jetbrains.buildServer.configs.kotlin.v2017_2.*

object Os_Citrix_BuildPackage : BuildType({
    uuid = "c35dfbb3-aeb8-4f87-b1c6-1193a8478d55"
    id = "Os_Citrix_BuildPackage"
    name = "Build Package"

    vcs {
        root(Os_Citrix.vcsRoots.Os_Citrix_HttpsBitbucketOrgOpenthinclientCitrixGit)

    }
})
