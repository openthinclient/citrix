package Os_Citrix.vcsRoots

import jetbrains.buildServer.configs.kotlin.v2017_2.*
import jetbrains.buildServer.configs.kotlin.v2017_2.vcs.GitVcsRoot

object Os_Citrix_HttpsBitbucketOrgOpenthinclientCitrixGit : GitVcsRoot({
    uuid = "4c72d451-3972-49e1-9cad-747be7d8167b"
    id = "Os_Citrix_HttpsBitbucketOrgOpenthinclientCitrixGit"
    name = "https://bitbucket.org/openthinclient/citrix.git"
    url = "https://bitbucket.org/openthinclient/citrix.git"
    authMethod = password {
        userName = "m.morvai@openthinclient.com"
        password = "credentialsJSON:3a1ff26b-9296-4d8f-bdbf-966a44447f0d"
    }
})
