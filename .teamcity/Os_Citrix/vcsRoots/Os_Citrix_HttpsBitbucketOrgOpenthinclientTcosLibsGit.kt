package Os_Citrix.vcsRoots

import jetbrains.buildServer.configs.kotlin.v2017_2.*
import jetbrains.buildServer.configs.kotlin.v2017_2.vcs.GitVcsRoot

object Os_Citrix_HttpsBitbucketOrgOpenthinclientTcosLibsGit : GitVcsRoot({
    uuid = "298abe85-ac10-471a-a177-52c0c51b2483"
    id = "Os_Citrix_HttpsBitbucketOrgOpenthinclientTcosLibsGit"
    name = "https://bitbucket.org/openthinclient/tcos-libs.git"
    url = "https://bitbucket.org/openthinclient/tcos-libs.git"
    authMethod = password {
        userName = "m.morvai@openthinclient.com"
        password = "credentialsJSON:9b09f45e-9317-48ec-9bc7-da7e1d94cb0f"
    }
})
