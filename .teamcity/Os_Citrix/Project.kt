package Os_Citrix

import Os_Citrix.buildTypes.*
import Os_Citrix.vcsRoots.*
import jetbrains.buildServer.configs.kotlin.v2017_2.*
import jetbrains.buildServer.configs.kotlin.v2017_2.Project
import jetbrains.buildServer.configs.kotlin.v2017_2.projectFeatures.VersionedSettings
import jetbrains.buildServer.configs.kotlin.v2017_2.projectFeatures.versionedSettings

object Project : Project({
    uuid = "b47463a2-2b09-4558-ab1c-22682796b0ed"
    id = "Os_Citrix"
    parentId = "Os"
    name = "Citrix"

    vcsRoot(Os_Citrix_HttpsBitbucketOrgOpenthinclientCitrixGit)
    vcsRoot(Os_Citrix_HttpsBitbucketOrgOpenthinclientTcosLibsGit)

    buildType(Os_Citrix_BuildPackage)

    features {
        versionedSettings {
            id = "PROJECT_EXT_2"
            mode = VersionedSettings.Mode.ENABLED
            buildSettingsMode = VersionedSettings.BuildSettingsMode.PREFER_SETTINGS_FROM_VCS
            rootExtId = "Os_HttpsMMorvaiBitbucketOrgOpenthinclientCitrixGit"
            showChanges = false
            settingsFormat = VersionedSettings.Format.KOTLIN
            storeSecureParamsOutsideOfVcs = true
        }
    }
})
