#!/bin/bash
Elux=`command -v eluxman > /dev/null 2>&1 && eluxman -qa | grep eluxmgmt`
ThinPro=`command -v dpkg > /dev/null 2>&1 && dpkg -l | grep thinpro-base | cut -d ' ' -f3`
CitrixConfigFile=/opt/Citrix/ICAClient/config/module.ini
if [ -n "$Elux" ]; then
    CitrixConfigFile=/setup/ica/module.ini
elif [ -n "$ThinPro" ]; then
    CitrixConfigFile=/usr/lib/ICAClient/config/module.ini
fi

addVirtualDriverItem()
{
    addedDriverItem=$1
    logPath="${2}"
    virtualDrivers=`grep $addedDriverItem ${CitrixConfigFile} | grep "VirtualDriver *="`
    if [ -z "$virtualDrivers" ]; then
        sed -i "s/\(VirtualDriver *= *\)\(.*\)/\1${addedDriverItem}, \2\n${addedDriverItem} = On/" ${CitrixConfigFile}
    else
        echo "$addedDriverItem already inserted into module.ini file" >> "${logPath}"
    fi
}

removeVirtualDriverItem()
{
    romovedDirverItem=$1
    sed -i "s/\(VirtualDriver *=.*\)${romovedDirverItem}, \(.*\)/\1\2/" ${CitrixConfigFile}
    sed -i "/${romovedDirverItem} = On/d" ${CitrixConfigFile}
}

addVirtualDriverDll()
{
    addedDriverItem=$1
    dllName=$2
    dllPath=$3$2
    addedDriverDllTag=`grep -n "\[$addedDriverItem\]" ${CitrixConfigFile}`
    if [ -z "$addedDriverDllTag" ]; then
        dllData="\[${addedDriverItem}\]\n;DriverName = ${dllPath}\nDriverName = ${dllName}\n\\n\[TransportDriver\]"
        sed -i "s/\[TransportDriver\]/${dllData}/g" ${CitrixConfigFile}
    fi
}

removeVirtualDriverDll()
{
    addedDriverItem=$1
    dllName=$2
    dllPath=$3
    DeleteLineNum=`sed -n "/.*\[${addedDriverItem}\].*/=" ${CitrixConfigFile}`
    sed -i "/\[${addedDriverItem}\].*/d" ${CitrixConfigFile}
    sed -i "/;DriverName = ${dllPath}.*/d" ${CitrixConfigFile}
    sed -i "/DriverName = ${dllName}.*/d" ${CitrixConfigFile}
    if [ -n "$DeleteLineNum" ]; then
        EmptyLine=`sed -n "${DeleteLineNum}p" ${CitrixConfigFile}`
        if [ -z "$EmptyLine" ]; then
            sed -i "${DeleteLineNum}d" ${CitrixConfigFile}
        fi
    fi
}

VirtualDriverTeamsPluginDll=libCiscoTeamsCitrixPlugin.so
VirtualDriverMeetingsPluginDll=libCiscoMeetingsCitrixPlugin.so
VirtualDriverDllPath="\/opt\/Citrix\/ICAClient\/"
VirtualDriverLogPath="/var/log/ciscouvdi/pluginInstall.log"

runScript()
{
    virtualDriver=""
    virtualDriverDll=""
    virtualDriverDllPath=$VirtualDriverDllPath
    logPath=${VirtualDriverLogPath}

    if [ ! -f "${logPath}" ]; then
      install -D /dev/null "${logPath}"
      chmod 777 /var/log/ciscouvdi/
    fi

    if [ "$1" == "-h" ]; then
      echo "Usage: ./$(basename $0) [add|remove] [Teams|Meetings]"
      echo "[add|remove]                 install or uninstall plugin"
      echo "[Teams|Meetings]             plugin for WebexTeams or WebexMeetings"
      exit 0
    fi

    if [ ! -e "$CitrixConfigFile" ]; then
        echo "Error: ${CitrixConfigFile} does not exist" >> "${logPath}"
        exit 0
    fi

    if [ "$2" = 'Teams' ]; then
        virtualDriver=CiscoTeamsVirtualChannel
        virtualDriverDll=$VirtualDriverTeamsPluginDll
    elif [ "$2" = 'Meetings' ]; then
        virtualDriver=CiscoMeetingsVirtualChannel
        virtualDriverDll=$VirtualDriverMeetingsPluginDll
    fi
    
    if [ "$1" = 'add' ]; then
        echo "$2 start inserted $virtualDriver into ${CitrixConfigFile} file" >> "${logPath}"
        addVirtualDriverItem $virtualDriver "${logPath}"
        addVirtualDriverDll $virtualDriver $virtualDriverDll $virtualDriverDllPath
        echo "$2 done inserted $virtualDriver into ${CitrixConfigFile} file" >> "${logPath}"
    elif [ "$1" = 'remove' ]; then
        removeVirtualDriverItem $virtualDriver
        removeVirtualDriverDll  $virtualDriver $virtualDriverDll $virtualDriverDllPath
        echo "done remove $virtualDriver from ${CitrixConfigFile} file" >> "${logPath}"
    fi
}

runScript $1 $2
