#!/bin/bash

for fd in $(ls /proc/$$/fd); do
    case "$fd" in
        0|1|2|255)
            ;;
        *)
            eval "exec $fd>&-"
            ;;
    esac
done

export GST_DEBUG='*:2'  # Surpress annoying logs in console
export LIB_PATH=/opt/cisco/TeamsVDI
export LD_PRELOAD=$LIB_PATH/libcrypto.so.1.1:$LIB_PATH/libssl.so.1.1
export LD_LIBRARY_PATH=$LIB_PATH:$LD_LIBRARY_PATH


# Remove those variable because they prevent normal start
unset DESKTOP_SESSION
unset GNOME_DESKTOP_SESSION_ID

LogToSTDOUT() {
    CONTENT="`date +%Y-%m-%d%H:%M:%S---`${1}"
    echo $CONTENT
}



# Start thin client if it is not runnning.

if [ -z $(pgrep -f /opt/cisco/TeamsVDI/webex_vdi) ] && [ -z "$(pidof webex_vdi)" ]; then
    LogToSTDOUT "Starting teamsvdi ..."
    cd /opt/cisco/TeamsVDI
    /opt/cisco/TeamsVDI/webex_vdi $* &
fi
